var $$ = document.querySelectorAll.bind(document);

var playButtons = $$(".playButton");

playButtons.forEach(function(button) {
    button.addEventListener('click', function(e) {
        e.preventDefault();
        console.log("Clicked");
        var video = this.offsetParent.querySelector('video');
        var isPlaying = video.getAttribute('is-playing');
        console.log("Isplaying", isPlaying);
        if (isPlaying == 'true') {
            video.setAttribute("is-playing", false);
            video.pause();
            return;
        }
        video.play();
        video.onplaying = function() {
            video.setAttribute("is-playing", true);
        }
    });
})